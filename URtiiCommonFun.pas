unit URtiiCommonFun;

interface

uses
  System.Generics.Defaults, System.Generics.Collections, System.TypInfo, System.SysUtils,
  Rtti, System.Variants, System.Classes;

const
  C_PUSHTYPE = 'pushtype';
  C_TYPE = 'type';

type
  TPropertyDic = TDictionary<string, string>;

function GetPropertyList(obj: TObject; dic: TPropertyDic): Boolean;

implementation

// 获取对象的 RTTI 属性与事件的函数
function GetPropertyList(obj: TObject; dic: TPropertyDic): Boolean;
var
  objProp: Variant;

  ctx: TRttiContext;
  T: TRttiType;
  p: TRttiProperty;
begin
  Result := false;

  if (Assigned(obj)) and (Assigned(dic)) then
  begin

    T := ctx.GetType(obj.ClassInfo);

    for p in T.GetProperties do
    begin
      try
        objProp := GetPropValue(obj, p.Name);
        if (VarIsStr(objProp)) or (VarIsNumeric(objProp) and (objProp <> 0)) then
        begin
          dic.Add(p.Name, VarToStr(objProp));
        end;
      except
      end;
    end;

    Result := true;
  end;

end;



end.
