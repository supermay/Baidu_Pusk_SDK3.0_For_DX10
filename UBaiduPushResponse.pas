unit UBaiduPushResponse;

interface

uses
  System.Generics.Collections, System.SysUtils, System.TypInfo;

type
  /// <summary>
  /// 百度云推送回复的基础类
  /// </summary>
  /// <typeparam name="T"></typeparam>
  TBdResponse<T> = class
  public
    request_id: Cardinal;
    error_code: Cardinal;
    error_msg: string;
    response_params: T;

    constructor Create(params: T);

    destructor Destroy; override;
  end;

  Tmsg_response = class
  public
    msg_id: String;
    send_time: Cardinal;

    function SendTimeToTime(): TDateTime;
  end;

  // <summary>
  /// 推送消息到单台设备返回值
  /// 使用 new BdResponse<Push_Single_Device_Response>(new Push_Single_Device_Response());
  /// </summary>
  TPush_Single_Device_Response = Tmsg_response;

  /// <summary>
  /// 推送广播消息到全部设备返回值
  /// </summary>
  TPush_All_Response = class(Tmsg_response)
  public
    timer_id: String; // string 否 	定时任务ID间
  end;

  /// <summary>
  /// 推送消息或通知给指定的标签返回值
  /// </summary>
  TPush_Tags_Response = class(Tmsg_response)
  public
    timer_id: String; // string 否 	定时任务ID间
  end;

  /// <summary>
  /// 推送消息到给定的一组设备(批量单播)返回值
  /// </summary>
  TPush_Batch_Device_Response = Tmsg_response;

  /// <summary>
  /// 查询消息的发送状态返回值
  /// </summary>
  Tquery_msg_status_array = class(Tmsg_response)
  public
    status: Cardinal; // number 	是 	取值如下：0：已发送；1：未发送；2：正在发送；3：失败；
    success: Cardinal; // number 	是 	成功到达数
  end;

  TReport_Query_Msg_Status_Response = class
  public
    total_num: Cardinal; // number 	是 	结果数量
    result: TList<Tquery_msg_status_array>; // JsonArray 	是 	result是数组对象，每项内容为一条消息的状态

    constructor Create();
  end;

  /// <summary>
  /// 查询定时消息的发送记录返回值
  /// </summary>
  Tquery_timer_records_array = class(Tmsg_response)
  public
    status: Cardinal; // number 	是 	取值如下：0：已发送；1：未发送；2：正在发送；3：失败；
  end;

  TReport_Query_Timer_Records_Response = class
  public
    timer_id: string; // string 	是 	定时任务ID
    result: TList<Tquery_timer_records_array>;
    // JsonArray 	是 	result是数组对象，每项内容为该定时任务所产生的一条消息的状态

    constructor Create();
  end;

  /// <summary>
  /// 查询指定分类主题的发送记录返回值
  /// </summary>
  Tquery_topic_records_array = class(Tmsg_response)
  public
    status: Cardinal; // number 	是 	取值如下：0：已发送；1：未发送；2：正在发送；3：失败；
  end;

  TReport_Query_Topic_Records_Response = class
  public
    topic_id: string; // string 	是 	定时任务ID
    result: TList<Tquery_topic_records_array>; // JsonArray 	是 	result是数组对象，每项内容为该定时任务所产生的一条消息的状态

    constructor Create();
  end;

  /// <summary>
  /// 查询标签组列表返回值
  /// </summary>
  Tquery_tags_array = class
  public
    tid: Cardinal; // number 	标签ID
    tag: string; // string 	标签名
    info: String; // string 	用于进一步描述标签的附件信息
    iType: Cardinal; // number 	标签类型（已废弃，请勿依赖该字段进行开发）
    createtime: Cardinal; // number 	标签创建时间，unix时间戳

    function CreateTimeToTime(): TDateTime;
  end;

  TApp_Query_Tags_Response = class
  public
    total_num: Cardinal; // number 	Tag总数
    result: TList<Tquery_tags_array>; // JsonArray 	是 	result是数组对象，每项内容为该定时任务所产生的一条消息的状态

    constructor Create();
  end;

  /// <summary>
  /// 创建标签组返回值
  /// </summary>
  TApp_Create_Tag_Response = class
  public
    tag: string; // string 	标签名
    result: Cardinal; // number 	状态 0：创建成功； 1：创建失败；
  end;

  /// <summary>
  /// 删除标签组返回值
  /// </summary>
  TApp_Del_Tag_Response = TApp_Create_Tag_Response;

  /// <summary>
  /// 添加设备到标签组返回值
  /// </summary>
  Tadd_devices_array = class
  public
    channel_id: string; // string 	设备ID
    result: Cardinal; // number 	状态 0：添加成功； 1：添加失败；
  end;

  TTag_Add_Devices_Response = class
  public
    result: TList<Tadd_devices_array>; // JsonArray 	是 	result是数组对象，每项内容为该定时任务所产生的一条消息的状态

    constructor Create();
  end;

  /// <summary>
  /// 将设备从标签组中移除
  /// </summary>
  Tdel_devices_array = Tadd_devices_array;

  TTagDel_devices_response = TTag_Add_Devices_Response;

  /// <summary>
  /// 查询标签组设备数量返回值
  /// </summary>
  TTag_Device_Num_Response = class
  public
    device_num: Cardinal; // number 	标签中设备的数量
  end;

  /// <summary>
  /// 查询定时任务列表返回值
  /// </summary>
  Ttimer_query_list_array = class
  public
    timer_id: string; // string 	发送定时消息时返回的timer_id
    msg: string; // string 	发送消息的内容
    send_time: Cardinal; // number 	消息的实际推送时间
    msg_type: Cardinal; // number 	消息类型：0：透传消息；1：通知；2：带格式的消息；3：富媒体消息；
    range_type: Cardinal; // number 	消息发送范围： 0：tag组播；1：广播；2：批量单播；3：标签组合；4：精准推送；5：LBS推送；6：系统保留；7：单播；
  end;

  TTimer_Query_List_Response = class
  public
    total_num: Cardinal; // number 	Tag总数
  public
    result: TList<Ttimer_query_list_array>;
    // JsonArray 	是 	result是数组对象，每项内容为该定时任务所产生的一条消息的状态

    constructor Create();
  end;

  /// <summary>
  /// 取消定时任务返回值
  /// void，如果未产生异常信息，则表示删除成功。
  /// </summary>
  TTimer_Cancel_Response = class
    // 这个返回值特殊的，但估计还是可以用基础类
    // {
    // "request_id":12394838223,
    // }
  end;

  /// <summary>
  /// 功能：查询推送过程中使用过的分类主题列表
  /// </summary>
  TTopic_Query_List_Array = class
  public
    ack_cnt: Cardinal; // number 	总的到达数，-1代表数据未ready
    ctime: Cardinal; // number 	第一次发送时间
    mtime: Cardinal; // number 	最后一次发送时间
    push_cnt: Cardinal; // number 	总的推送目标数
    topic_id: string; // string 	分类主题名称
  end;

  TTopic_Query_List_Response = class
  public
    total_num: Cardinal; // number 	Tag总数
    result: TList<TTopic_Query_List_Array>; // JsonArray 	是 	result是数组对象，每项内容为该定时任务所产生的一条消息的状态

    constructor Create();
  end;

  /// <summary>
  /// 当前应用的设备统计信息返回值
  /// </summary>
  TReport_Statistic_Device_Map_Value = class

  public
    new_term: Cardinal; // 当天新增用户数；
    del_term: Cardinal; // 当天解绑用户数；
    online_term: Cardinal; // 当天在线用户数；
    addup_term: Cardinal; // 当天累计终端数；
    total_term: Cardinal; // 有效channelId总数，等于addup_term 减去 del_term；
  end;

  TReport_Statistic_Device_Response = class
  public
    total_num: Cardinal; // number 	统计结果集的条数
    result: TDictionary<Cardinal, TReport_Statistic_Device_Map_Value>;
    // result属性是一个map对象，其中：key为统计信息所在当天0点0分时间戳；

    constructor Create();
  end;

  /// <summary>
  /// 查询分类主题统计信息返回值
  /// 统计当前应用下一个分类主题的消息数量
  /// </summary>
  TReport_Statistic_Topic_Map_Value = class
  public
    ack: Cardinal; // 当天消息到达数
  end;

  TReport_Statistic_Topic_Response = class
  public
    total_num: Cardinal; // number 	统计结果集的条数
    result: TDictionary<Cardinal, TReport_Statistic_Topic_Map_Value>;
    // result属性是一个map对象，其中：key为统计信息所在当天0点0分时间戳；

    constructor Create();
  end;

implementation

uses
  System.DateUtils;

{ Tmsg_response }

function Tmsg_response.SendTimeToTime: TDateTime;
begin
  result := UnixToDateTime(send_time, False);
end;

{ TReport_Query_Msg_Status_Response }

constructor TReport_Query_Msg_Status_Response.Create;
begin
  result := TList<Tquery_msg_status_array>.Create();
end;

{ TReport_Query_Timer_Records_Response }

constructor TReport_Query_Timer_Records_Response.Create;
begin
  result := TList<Tquery_timer_records_array>.Create();
end;

{ TReport_Query_Topic_Records_Response }

constructor TReport_Query_Topic_Records_Response.Create;
begin
  result := TList<Tquery_topic_records_array>.Create();
end;

{ TApp_Query_Tags_Response }

constructor TApp_Query_Tags_Response.Create;
begin
  result := TList<Tquery_tags_array>.Create();
end;

{ Tquery_tags_array }

function Tquery_tags_array.CreateTimeToTime: TDateTime;
begin
  result := UnixToDateTime(createtime, False);
end;

{ TTag_Add_Devices_Response }

constructor TTag_Add_Devices_Response.Create;
begin
  result := TList<Tadd_devices_array>.Create();
end;

{ TTimer_Query_List_Response }

constructor TTimer_Query_List_Response.Create;
begin
  result := TList<Ttimer_query_list_array>.Create();
end;

{ TTopic_Query_List_Response }

constructor TTopic_Query_List_Response.Create;
begin
  result := TList<TTopic_Query_List_Array>.Create();
end;

{ TReport_Statistic_Device_Response }

constructor TReport_Statistic_Device_Response.Create;
begin
  result := TDictionary<Cardinal, TReport_Statistic_Device_Map_Value>.Create();
end;

{ TReport_Statistic_Topic_Response }

constructor TReport_Statistic_Topic_Response.Create;
begin
  result := TDictionary<Cardinal, TReport_Statistic_Topic_Map_Value>.Create();
end;

{ TBdResponse<T> }

constructor TBdResponse<T>.Create(params: T);
begin
  response_params := params;
end;

destructor TBdResponse<T>.Destroy;
var
  p: PTypeInfo;
begin
  p := System.TypeInfo(T);
  if (p.Kind in [tkClass, tkClassRef]) then
  begin
    FreeAndNil(response_params);
  end;
  inherited;
end;

end.
