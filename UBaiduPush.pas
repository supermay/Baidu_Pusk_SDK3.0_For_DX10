{ ******************************************************* }
{ }
{ 百度云推送3.0 SDK For DX10 }
{ }
{ 版权所有 (C) 2016 闲人工作室 }
{ }
{ ******************************************************* }

unit UBaiduPush;

interface

uses
  Generics.Collections, System.Classes, System.TypInfo, System.Variants, System.SysUtils,
  System.Generics.Defaults, System.Net.URLClient, JsonDataObjects, URtiiCommonFun,
  System.Net.HttpClient, System.Threading, System.Hash, UBaiduPushResponse;

type

  // 设备类型
  TDevice_Type = (dtAndroid = 3, dtIOS = 4);
  /// 推送类型
  TPush_Type = (ptSignle = 1, ptMultiple = 2, ptAll = 3);
  /// 消息类型
  TMessage_Type = (mtMessage = 0, mtNotice = 1);
  /// 部署状态
  TDeploy_Status = (dsDevelope = 1, dsProduct = 2);
  // 0：tag组播； 1：广播； 2：批量单播； 3：标签组合；
  // 4：精准推送； 5：LBS推送； 6：系统保留； 7：单播；
  TQuery_Type = (qtTagPush = 0, qtPush = 1, qtBatchPush = 2, qtTagsPush = 3,
    qtPrecisionPush = 4, qtLBSPush = 5, qtSysKeep = 6, qtSignalPush = 7);

  TBaidu_Mod = class
  private
    // string 	是 	应用的api key,用于标识应用。
    Fapikey: string;
    // uint 	    是 	用户发起请求时的unix时间戳。本次请求签名的有效时间为该时间戳向后10分钟。
    Ftimestamp: Cardinal;
    // string 	是 	调用参数签名值，与apikey成对出现。用于防止请求内容被篡改, 生成方法请参考。
    Fsign: string;
    // uint 	    否 	用户指定本次请求签名的失效时间。格式为unix时间戳形式，
    // 用于防止 replay 型攻击。为保证防止 replay攻击算法的正确有效，请保证客户端系统时间正确。
    Fexpires: Cardinal;
    // uint  	否 	当一个应用同时支持多个设备平台类型（比如：Android和iOS），请务必设置该参数。
    // 其余情况可不设置。具体请参见：device_type参数使用说明，android:3,ios:4
    Fdevice_type: Cardinal;
  published
    property apikey: String read Fapikey write Fapikey;
    property timestamp: Cardinal read Ftimestamp write Ftimestamp;
    property sign: String read Fsign write Fsign;
    property expires: Cardinal read Fexpires write Fexpires;
    property device_type: Cardinal read Fdevice_type write Fdevice_type;

    constructor Create(Aapikey: String);
  end;

  TBaidu_Helper = class
  public

    const
    HTTP_POST: string = 'post'; // post传输
    HTTP_GET: string = 'get'; // get传输
    // 域名：api.push.baidu.com、api.tuisong.baidu.com、channel.api.duapp.com(老域名，建议使用前两个域名)
    BASE_URL: string = 'http://api.tuisong.baidu.com/rest/3.0/';

    PUSH_SIGNLE_DEVICE: string = 'push/single_device'; // 推送消息到单台设备
    PUSH_ALL: string = 'push/all'; // 推送广播消息
    PUSH_TAGS: string = 'push/tags'; // 推送组播消息
    PUSH_BATCH_DEVICE: string = 'push/batch_device'; // 推送消息到给定的一组设备(批量单播)
    REPORT_QUERY_MSG_STATUS: string = 'report/query_msg_status'; // 查询消息的发送状态
    REPORT_QUERY_TIMER_RECORDS: string = 'report/query_timer_records'; // 查询定时消息的发送记录
    REPORT_QUERY_TOPIC_RECORDS: string = 'report/query_topic_records'; // 查询指定分类主题的发送记录
    APP_QUERY_TAGS: string = 'app/query_tags'; // 查询标签组列表
    App_Create_Tag: string = 'app/create_tag'; // 创建标签组
    APP_DEL_TAG: string = 'app/del_tag'; // 删除标签组
    TAG_ADD_DEVICES: string = 'tag/add_devices'; // 添加设备到标签组
    TAG_DEL_DEVICES: string = 'tag/del_devices'; // 将设备从标签组中移除
    TAG_DEVICE_NUM: string = 'tag/device_num'; // 查询标签组设备数量
    TIMER_QUERY_LIST: string = 'timer/query_list'; // 查询定时任务列表
    TIMER_CANCEL: string = 'timer/cancel'; // 取消定时任务
    TOPIC_QUERY_LIST: string = 'topic/query_list'; // 查询分类主题列表
    REPORT_STATISTIC_MSG: string = 'report/statistic_msg'; // 当前应用的消息统计信息
    REPORT_STATISTIC_DEVICE: string = 'report/statistic_device'; // 当前应用的设备统计信息
    REPORT_STATISTIC_TOPIC: string = 'report/statistic_topic'; // 查询分类主题统计信息

  public
    class
      function GetParamerCollection(Baidu_Mod: TBaidu_Mod;
      sign: String): TPropertyDic;
      overload;
    class
      function CreateSign(httpMethod: string; url: string;
      secretKey: string; Baidu_Mod: TBaidu_Mod): string;
    class
      function SendBaidu(httpMethod, url, secretKey: string; Baidu_Mod: TBaidu_Mod): IFuture<string>;
  public
    device_type: TDevice_Type;
    Push_Type: TPush_Type;
    Message_Type: TMessage_Type;
    Deploy_Status: TDeploy_Status;
    Query_Type: TQuery_Type;
  end;

  TAbstractBaidu_Send<T> = class
  public
    Baidu_Mod: TBaidu_Mod;
    url: string;
    httpMehtod: string;
    secret_key: string;
    delay: Word;

    constructor Create(ABaidu_Mod: TBaidu_Mod; Asecret_key, AUrl: string); overload;
    constructor Create(ABaidu_Mod: TBaidu_Mod; Asecret_key, AUrl, AhttpMehtod: string); overload;

    function PushMessage(): string;

    function PushMessageAndResponse(): TBdResponse<T>; virtual; abstract;
  end;

  /// 功能：创建一个空的标签组
  TApp_Create_Tag_Mod = class(TBaidu_Mod)
  private
    Ftag: string;
  published
    // 是 	1~128字节，但不能为‘default’ 	标签名称
    property tag: string read Ftag write Ftag;
  public
    constructor Create(Aapikey, Atag: String); overload;
  end;

  TApp_Create_Tag = class(TAbstractBaidu_Send<TApp_Create_Tag_Response>)
  public
    constructor Create(ABaidu_Mod: TBaidu_Mod; Asecret_key: string; AUrl: string = ''); overload;

    function PushMessageAndResponse(): TBdResponse<TApp_Create_Tag_Response>; override;
  end;

  /// 功能：删除一个已存在的tag
  TApp_Del_Tag_Mod = TApp_Create_Tag_Mod;

  TApp_Del_Tag = class(TAbstractBaidu_Send<TApp_Del_Tag_Response>)
  public
    constructor Create(ABaidu_Mod: TBaidu_Mod; Asecret_key: string; AUrl: string = ''); overload;

    function PushMessageAndResponse(): TBdResponse<TApp_Del_Tag_Response>; override;
  end;

  /// 功能：查询应用的tag
  TApp_Query_Tags_Mod = class(TApp_Create_Tag_Mod)
  private
    Fstart: Cardinal;
    Flimit: Cardinal;
  published
    // 否 	整数，默认为0 	指定返回记录的起始索引位置
    property start: Cardinal read Fstart write Fstart;
    // 否 	整数：[1,100]，默认100 	返回的记录条数
    property limit: Cardinal read Flimit write Flimit;
  public
    // Atag 否 	1-128字节，不传则获取应用下所有标签信息 	标签名称
    constructor Create(Aapikey, Atag: String); overload;
    constructor Create(Aapikey: String; Astart, Alimit: Cardinal); overload;
  end;

  TApp_Query_Tags = class(TAbstractBaidu_Send<TApp_Query_Tags_Response>)
  public
    constructor Create(ABaidu_Mod: TBaidu_Mod; Asecret_key: string; AUrl: string = ''); overload;

    function PushMessageAndResponse(): TBdResponse<TApp_Query_Tags_Response>; override;
  end;

  /// 功能：向当前app下所有设备推送一条消息
  TPush_All_Mod = class(TBaidu_Mod)
  private
    Fmsg: string;
    Fmsg_type: Cardinal;
    Fdeploy_status: Cardinal;
    Fmsg_expires: Cardinal;
    Fsend_time: Cardinal;
  published
    // 否 	取值如下：0：消息；1：通知。默认为0 	消息类型
    property msg_type: Cardinal read Fmsg_type write Fmsg_type;
    // 是 	详情见消息/通知数据格式 	消息内容，json格式
    property msg: string read Fmsg write Fmsg;
    // 否 	0~604800(86400*7)，默认为5小时(18000秒) 	相对于当前时间的消息过期时间，单位为秒
    property msg_expires: Cardinal read Fmsg_expires write Fmsg_expires;
    // 否 	取值为：1：开发状态；2：生产状态； 若不指定，则默认设置为生产状态。 	设置iOS应用的部署状态，仅iOS应用推送时使用
    property Deploy_Status: Cardinal read Fdeploy_status write Fdeploy_status;
    // 否  指定的实际发送时间，必须在当前时间60s以外，1年以内 	定时推送，用于指定的实际发送时间
    property send_time: Cardinal read Fsend_time write Fsend_time;
  public
    constructor Create(Aapikey, Amsg: String); overload;
    constructor Create(Aapikey, Amsg: String; Amsg_type: Cardinal); overload;
  end;

  TPush_All = class(TAbstractBaidu_Send<TPush_All_Response>)
  public
    constructor Create(ABaidu_Mod: TBaidu_Mod; Asecret_key: string; AUrl: string = ''); overload;

    function PushMessageAndResponse(): TBdResponse<TPush_All_Response>; override;
  end;

  /// 功能：推送消息给批量设备（批量单播）
  TPush_Batch_Device_Mod = class(TBaidu_Mod)
  private
    Fchannel_ids: string; // string 	是 	一组channel_id（最多为一万个）组成的json数组字符串 	对应一批设备
    Fmsg_type: Cardinal; // number 	否 	取值如下：0：消息；1：通知。默认为0 	消息类型
    Fmsg: string; // string 	是 	详情见消息/通知数据格式 	消息内容，json格式
    Fmsg_expires: Cardinal; // number 	否 	0~604800(86400*7)，默认为5小时(18000秒) 	相对于当前时间的消息过期时间，单位为秒
    Ftopic_id: string; // string 	是 	字母、数字及下划线组成，长度限制为1~128 	分类主题名称
  published
    property channel_ids: string read Fchannel_ids write Fchannel_ids;
    property msg_type: Cardinal read Fmsg_type write Fmsg_type;
    property msg: string read Fmsg write Fmsg;
    property msg_expires: Cardinal read Fmsg_expires write Fmsg_expires;
    property topic_id: string read Ftopic_id write Ftopic_id;
  public
    constructor Create(Aapikey, Achannel_ids, Amsg, Atopic_id: String); overload;
    constructor Create(Aapikey, Achannel_ids, Amsg, Atopic_id: String; msg_type: Cardinal); overload;
  end;

  TPush_Batch_Device = class(TAbstractBaidu_Send<TPush_Batch_Device_Response>)
  public
    constructor Create(ABaidu_Mod: TBaidu_Mod; Asecret_key: string; AUrl: string = ''); overload;

    function PushMessageAndResponse(): TBdResponse<TPush_Batch_Device_Response>; override;
  end;

  /// 功能：根据给定的channel_id推送消息给单个设备
  TPush_Single_Device_Mod = class(TBaidu_Mod)
  private
    Fchannel_id: string; // string 	是 	必须为端上初始化channel成功之后返回的channel_id 	唯一对应一台设备
    Fmsg_type: Cardinal; // number 	否 	取值如下：0：消息；1：通知。默认为0 	消息类型
    Fmsg: string; // string 	是 	详情见消息/通知数据格式 	消息内容，json格式
    Fmsg_expires: Cardinal; // number 	否 	0~604800(86400*7)，默认为5小时(18000秒) 	相对于当前时间的消息过期时间，单位为秒
    Fdeploy_status: Cardinal; // number 	否 	取值为：1：开发状态；2：生产状态； 若不指定，则默认设置为生产状态。 	设置iOS应用的部署状态，仅iOS应用推送时使用
  published
    property channel_id: string read Fchannel_id write Fchannel_id;
    property msg_type: Cardinal read Fmsg_type write Fmsg_type;
    property msg: string read Fmsg write Fmsg;
    property msg_expires: Cardinal read Fmsg_expires write Fmsg_expires;
    property Deploy_Status: Cardinal read Fdeploy_status write Fdeploy_status;
  public
    constructor Create(Aapikey, Achannel_id, Amsg: String); overload;
    constructor Create(Aapikey, Achannel_id, Amsg: String; Amsg_type: Cardinal); overload;
  end;

  TPush_Single_Device = class(TAbstractBaidu_Send<TPush_Single_Device_Response>)
  public
    constructor Create(ABaidu_Mod: TBaidu_Mod; Asecret_key: string; AUrl: string = ''); overload;

    function PushMessageAndResponse(): TBdResponse<TPush_Single_Device_Response>; override;
  end;

  /// 功能：推送消息或通知给指定的标签
  TPush_Tags_Mod = class(TBaidu_Mod)
  private
    Ftype: Cardinal; // number 	是 	目前固定值为 1 	推送的标签类型
    Ftag: string; // string 	是 	已创建的tag名称 	标签名
    Fmsg_type: Cardinal; // number 	否 	取值如下：0：消息；1：通知。默认为0 	消息类型
    Fmsg: string; // string 	是 	详情见消息/通知数据格式 	消息内容，json格式
    Fmsg_expires: Cardinal; // number 	否 	0~604800(86400*7)，默认为5小时(18000秒) 	相对于当前时间的消息过期时间，单位为秒
    Fdeploy_status: Cardinal; // number 	否 	取值为：1：开发状态；2：生产状态； 若不指定，则默认设置为生产状态。 	设置iOS应用的部署状态，仅iOS应用推送时使用
    Fsend_time: Cardinal; // number    否  指定的实际发送时间，必须在当前时间60s以外，1年以内 	定时推送，用于指定的实际发送时间

  published
    property pushtype: Cardinal read Ftype write Ftype;
    property tag: string read Ftag write Ftag;
    property msg_type: Cardinal read Fmsg_type write Fmsg_type;
    property msg: string read Fmsg write Fmsg;
    property msg_expires: Cardinal read Fmsg_expires write Fmsg_expires;
    property Deploy_Status: Cardinal read Fdeploy_status write Fdeploy_status;
  public
    constructor Create(Aapikey, Atag, Amsg: String); overload;
    constructor Create(Aapikey, Atag, Amsg: String; Amsg_type: Cardinal); overload;
  end;

  TPush_Tags = class(TAbstractBaidu_Send<TPush_Tags_Response>)
  public
    constructor Create(ABaidu_Mod: TBaidu_Mod; Asecret_key: string; AUrl: string = ''); overload;

    function PushMessageAndResponse(): TBdResponse<TPush_Tags_Response>; override;
  end;

  /// 功能：根据msg_id获取消息推送报告
  TReport_Query_Msg_Status_Mod = class(TBaidu_Mod)
  private
    Fmsg_id: string; // string 	是 	推送接口返回的msg_id，支持一个由msg_id组成的json数组 	消息ID
  published
    property msg_id: string read Fmsg_id write Fmsg_id;
  public
    constructor Create(Aapikey, Amsg_id: String); overload;
  end;

  TReport_Query_Msg_Status = class(TAbstractBaidu_Send<TReport_Query_Msg_Status_Response>)
  public
    constructor Create(ABaidu_Mod: TBaidu_Mod; Asecret_key: string; AUrl: string = ''); overload;

    function PushMessageAndResponse(): TBdResponse<TReport_Query_Msg_Status_Response>; override;
  end;

  /// 功能：根据timer_id获取消息推送记录
  TReport_Query_Timer_Records_Mod = class(TBaidu_Mod)
  private
    Ftimer_id: string; // string 	是 	推送接口返回的timer_id 	定时任务ID
    Fstart: Cardinal; // number 	否 	整数，默认为0 	指定返回记录的起始索引位置
    Flimit: Cardinal; // number 	否 	整数：[1,100]，默认100 	返回的记录条数
    Frange_start: Cardinal; // number 	否 	指定查询起始时间范围，以服务端实际推送时间为准；默认为七天前的0点时间 	unix时间戳
    Frange_end: Cardinal; // number 	否 	指定查询截止时间范围，以服务端实际推送时间为准；默认时间为当前时间 	unix时间戳

  published
    property timer_id: string read Ftimer_id write Ftimer_id;
    property start: Cardinal read Fstart write Fstart;
    property limit: Cardinal read Flimit write Flimit;
    property range_start: Cardinal read Frange_start write Frange_start;
    property range_end: Cardinal read Frange_end write Frange_end;
  public
    constructor Create(Aapikey, Atimer_id: String); overload;
  end;

  TReport_Query_Timer_Records = class(TAbstractBaidu_Send<TReport_Query_Timer_Records_Response>)
  public
    constructor Create(ABaidu_Mod: TBaidu_Mod; Asecret_key: string; AUrl: string = ''); overload;

    function PushMessageAndResponse(): TBdResponse<TReport_Query_Timer_Records_Response>; override;
  end;

  /// 功能：根据分类主题获取消息推送记录
  TReport_Query_Topic_Records_Mod = class(TBaidu_Mod)
  private
    Ftopic_id: string; // string 	是 	字母、数字及下划线组成，长度限制为1~128 	分类主题名称
    Fstart: Cardinal; // number 	否 	整数，默认为0 	指定返回记录的起始索引位置
    Flimit: Cardinal; // number 	否 	整数：[1,100]，默认100 	返回的记录条数
    Frange_start: Cardinal; // number 	否 	指定查询起始时间范围，以服务端实际推送时间为准；默认为七天前的0点时间 	unix时间戳
    Frange_end: Cardinal; // number 	否 	指定查询截止时间范围，以服务端实际推送时间为准；默认时间为当前时间 	unix时间戳

  published
    property topic_id: string read Ftopic_id write Ftopic_id;
    property start: Cardinal read Fstart write Fstart;
    property limit: Cardinal read Flimit write Flimit;
    property range_start: Cardinal read Frange_start write Frange_start;
    property range_end: Cardinal read Frange_end write Frange_end;
  public
    constructor Create(Aapikey, Atopic_id: String); overload;
  end;

  TReport_Query_Topic_Records = class(TAbstractBaidu_Send<TReport_Query_Topic_Records_Response>)
  public
    constructor Create(ABaidu_Mod: TBaidu_Mod; Asecret_key: string; AUrl: string = ''); overload;

    function PushMessageAndResponse(): TBdResponse<TReport_Query_Topic_Records_Response>; override;
  end;

  /// 功能：统计APP 设备数
  TReport_Statistic_Device_Mod = TBaidu_Mod;

  TReport_Statistic_Device = class(TAbstractBaidu_Send<TReport_Statistic_Device_Response>)
  public
    constructor Create(ABaidu_Mod: TBaidu_Mod; Asecret_key: string; AUrl: string = ''); overload;

    function PushMessageAndResponse(): TBdResponse<TReport_Statistic_Device_Response>; override;
  end;

  /// 功能：统计app消息信息
  TReport_Statistic_Msg_Mod = class(TBaidu_Mod)
  private
    // number 	否 	统计的消息类型状态，接收一个number或多个number组成的json数组
    // 0：tag组播； 1：广播；
    // 2：批量单播； 3：标签组合； 4：精准推送； 5：LBS推送； 6：系统保留； 7：单播；
    Fquery_type: Cardinal;
  published
    property Query_Type: Cardinal read Fquery_type write Fquery_type;
  end;

  // 官网文档没有这个
  TReport_Statistic_Msg<T> = class(TAbstractBaidu_Send<T>)
  public
    constructor Create(ABaidu_Mod: TBaidu_Mod; Asecret_key: string; AUrl: string = ''); overload;

    function PushMessageAndResponse(): TBdResponse<T>; override;
  end;

  /// 功能：统计当前应用下一个分类主题的消息数量
  TReport_Statistic_Topic_Mod = class(TBaidu_Mod)
  private
    Ftopic_id: string; // string 	是 	字母、数字及下划线组成，长度限制为1~128 	分类主题名称
  published
    property topic_id: string read Ftopic_id write Ftopic_id;
  public
    constructor Create(Aapikey, Atopic_id: String); overload;
  end;

  TReport_Statistic_Topic = class(TAbstractBaidu_Send<TReport_Statistic_Topic_Response>)
  public
    constructor Create(ABaidu_Mod: TBaidu_Mod; Asecret_key: string; AUrl: string = ''); overload;

    function PushMessageAndResponse(): TBdResponse<TReport_Statistic_Topic_Response>; override;
  end;

  /// 功能：向tag中批量添加设备
  TTag_Add_Devices_Mod = class(TBaidu_Mod)
  private
    Ftag: string;
    Fchannel_ids: string; // string 	是 	1~128字节，但不能为‘default’ 	标签名称
  published
    property tag: string read Ftag write Ftag;
    property channel_ids: string read Fchannel_ids write Fchannel_ids;
  public
    constructor Create(Aapikey, Atag, Achannel_ids: String); overload;
  end;

  TTag_Add_Devices = class(TAbstractBaidu_Send<TTag_Add_Devices_Response>)
  public
    constructor Create(ABaidu_Mod: TBaidu_Mod; Asecret_key: string; AUrl: string = ''); overload;

    function PushMessageAndResponse(): TBdResponse<TTag_Add_Devices_Response>; override;
  end;

  /// 功能：从tag中批量解绑设备
  TTag_Del_Devices_Mod = TTag_Add_Devices_Mod;

  TTag_Del_Devices = class(TAbstractBaidu_Send<TTagDel_devices_response>)
  public
    constructor Create(ABaidu_Mod: TBaidu_Mod; Asecret_key: string; AUrl: string = ''); overload;

    function PushMessageAndResponse(): TBdResponse<TTagDel_devices_response>; override;
  end;

  /// 功能：查询某个tag关联的设备数量
  TTag_Device_Num_Mod = class(TBaidu_Mod)
  private
    Ftag: string; // string 	是 	1~128字节，但不能为‘default’ 	标签名称
  published
    property tag: string read Ftag write Ftag;
  public
    constructor Create(Aapikey, Atag: String); overload;
  end;

  TTag_Device_Num = class(TAbstractBaidu_Send<TTag_Device_Num_Response>)
  public
    constructor Create(ABaidu_Mod: TBaidu_Mod; Asecret_key: string; AUrl: string = ''); overload;

    function PushMessageAndResponse(): TBdResponse<TTag_Device_Num_Response>; override;
  end;

  /// 功能：取消尚未执行的定时推送任务
  TTimer_Cancel_Mod = class(TBaidu_Mod)
  private
    Ftimer_id: string; // string 	否 	推送接口返回的timer_id唯一标识一个定时推送任务 	如果指定该参数，将仅返回该timer_id对应定时任务的相关信息
  published
    property timer_id: string read Ftimer_id write Ftimer_id;
  public
    constructor Create(Aapikey, Atimer_id: String); overload;
  end;

  TTimer_Cancel = class(TAbstractBaidu_Send<TTimer_Cancel_Response>)
  public
    constructor Create(ABaidu_Mod: TBaidu_Mod; Asecret_key: string; AUrl: string = ''); overload;

    function PushMessageAndResponse(): TBdResponse<TTimer_Cancel_Response>; override;
  end;

  /// 功能：查看还未执行的定时任务，每个应用可设置的有效的定时任务有限制(目前为10个)。
  TTimer_Query_List_Mod = class(TBaidu_Mod)
  private
    Ftimer_id: string; // string 	否 	推送接口返回的timer_id唯一标识一个定时推送任务 	如果指定该参数，将仅返回该timer_id对应定时任务的相关信息
    Fstart: string; // number 	否 	整数，默认为0 	指定返回记录的起始索引位置
    Flimit: string; // number 	否 	整数：[1,100]，默认100 	返回的记录条数
  published
    property timer_id: string read Ftimer_id write Ftimer_id;
    property start: string read Fstart write Fstart;
    property limit: string read Flimit write Flimit;
  public
    constructor Create(Aapikey, Atimer_id: String); overload;
  end;

  TTimer_Query_List = class(TAbstractBaidu_Send<TTimer_Query_List_Response>)
  public
    constructor Create(ABaidu_Mod: TBaidu_Mod; Asecret_key: string; AUrl: string = ''); overload;

    function PushMessageAndResponse(): TBdResponse<TTimer_Query_List_Response>; override;
  end;

  /// 功能：查询推送过程中使用过的分类主题列表
  TTopic_Query_List_Mod = class(TBaidu_Mod)
  private
    Fstart: string; // number 	否 	整数，默认为0 	指定返回记录的起始索引位置
    Flimit: string; // number 	否 	整数：[1,100]，默认100 	返回的记录条数
  published
    property start: string read Fstart write Fstart;
    property limit: string read Flimit write Flimit;
  end;

  TTopic_Query_List = class(TAbstractBaidu_Send<TTopic_Query_List_Response>)
  public
    constructor Create(ABaidu_Mod: TBaidu_Mod; Asecret_key: string; AUrl: string = ''); overload;

    function PushMessageAndResponse(): TBdResponse<TTopic_Query_List_Response>; override;
  end;

implementation

uses
  Rtti, System.DateUtils;

function getDefauleTimestamp(): Cardinal;
begin
  Result := DateTimeToUnix(Now, false);
end;

function GetParamsToStr(dic: TPropertyDic): String;
var
  ds: TPropertyDic.TPairEnumerator;
  myStrList: TStringList;
  keyName: string;
begin
  Result := '';

  myStrList := TStringList.Create;
  try
    if (Assigned(dic)) then
    begin
      ds := dic.GetEnumerator;
      while ds.MoveNext do
      begin
        keyName := ds.Current.Key;
        if keyName = C_PUSHTYPE then
          keyName := C_TYPE;

        myStrList.Add(format('%s=%s', [keyName, ds.Current.Value]));
      end;

      myStrList.Sort;
      Result := myStrList.Text.Replace(#13#10, '');
    end;
  finally
    myStrList.Free;
  end;
end;

function GetParams(dic: TPropertyDic): String;
var
  ds: TPropertyDic.TPairEnumerator;
  myStrList: TStringList;
  keyName: String;
begin
  Result := '';

  myStrList := TStringList.Create;
  try
    if (Assigned(dic)) then
    begin
      ds := dic.GetEnumerator;
      while ds.MoveNext do
      begin
        keyName := ds.Current.Key;
        if keyName = C_PUSHTYPE then
          keyName := C_TYPE;

        myStrList.Add(format('%s=%s', [keyName, ds.Current.Value]));
      end;

      myStrList.Sort;
      Result := myStrList.Text;
    end;
  finally
    myStrList.Free;
  end;

end;

{ TBaidu_Helper }

class function TBaidu_Helper.CreateSign(httpMethod, url, secretKey: string; Baidu_Mod: TBaidu_Mod)
  : string;
var
  dic: TPropertyDic;
  str: TStringStream;
  myParams: string;
  strSignUpper: string;
begin
  dic := TPropertyDic.Create();
  if (GetPropertyList(Baidu_Mod, dic)) then
  begin
    // 生成sign时，不能包含sign标签，所有移除
    dic.Remove('sign');

    myParams := GetParamsToStr(dic);
    /// / 按要求拼接字符串，并urlencode编码
    str := TStringStream.Create(TURI.URLEncode(httpMethod.ToUpper + url + myParams + secretKey),
      TEncoding.UTF8);
    try
      strSignUpper := str.DataString;
    finally
      str.Free;
    end;
    Result := THashMD5.GetHashString(strSignUpper).ToLower();
  end;

end;

class function TBaidu_Helper.GetParamerCollection(Baidu_Mod: TBaidu_Mod; sign: String): TPropertyDic;
begin
  Result := TPropertyDic.Create();

  if (GetPropertyList(Baidu_Mod, Result)) then
  begin
    Result.AddOrSetValue('sign', sign);
  end;

end;

class function TBaidu_Helper.SendBaidu(httpMethod, url, secretKey: string; Baidu_Mod: TBaidu_Mod)
  : IFuture<string>;
var
  requestParams: TPropertyDic;
  myParams: String;

  myHttp: THttpClient;
  responseS: TStringStream;
  rList: TStringList;
  strSign: String;
begin
  Result := nil;

  if (Assigned(Baidu_Mod)) then
  begin
    strSign := CreateSign(httpMethod, url, secretKey, Baidu_Mod);

    requestParams := GetParamerCollection(Baidu_Mod, strSign);
    myParams := GetParams(requestParams);

    Result := TTask.Future<string>(
      function: string
      begin
        myHttp := THttpClient.Create;

        rList := TStringList.Create;
        responseS := TStringStream.Create;
        try
          myHttp.ContentType := 'application/x-www-form-urlencoded; charset=utf-8';
          myHttp.Accept := 'application/json; charset=utf-8';
          myHttp.UserAgent :=
            'BCCS_SDK/3.0 (SAE LINUX ENVIRONMENT; SAE LINUX ENVIRONMENT; SAE LINUX ENVIRONMENT)'
            + ' PHP/5.3.29 (Baidu Push SDK for PHP v3.0.0) apache2handler/Unknown(Apache) ZEND/2.3.0';

          rList.Text := myParams;

          myHttp.Post(url, rList, responseS);

          Result := responseS.DataString;
        finally
          myHttp.Free;
          responseS.Free;
          rList.Free;
        end;
      end);

  end;
end;

{ TAbstractBaidu_Send }

constructor TReport_Query_Msg_Status.Create(ABaidu_Mod: TBaidu_Mod; Asecret_key: string; AUrl: string);
var
  url: String;
begin
  if AUrl = '' then
  begin
    url := TBaidu_Helper.BASE_URL;
  end
  else
  begin
    url := AUrl;
  end;
  url := url + TBaidu_Helper.REPORT_QUERY_MSG_STATUS;

  inherited Create(ABaidu_Mod, Asecret_key, url);
end;

{ TAbstractBaidu_Send }

constructor TAbstractBaidu_Send<T>.Create(ABaidu_Mod: TBaidu_Mod; Asecret_key, AUrl: string);
begin
  Create(ABaidu_Mod, Asecret_key, AUrl, TBaidu_Helper.HTTP_POST);
end;

constructor TAbstractBaidu_Send<T>.Create(ABaidu_Mod: TBaidu_Mod; Asecret_key, AUrl, AhttpMehtod: string);
begin
  self.httpMehtod := AhttpMehtod;
  self.url := AUrl;
  self.secret_key := Asecret_key;
  self.Baidu_Mod := ABaidu_Mod;
  self.delay := 3000; // 3S
end;

function TAbstractBaidu_Send<T>.PushMessage: string;
var
  myTask: IFuture<string>;
begin
  myTask := TBaidu_Helper.SendBaidu(self.httpMehtod, self.url, self.secret_key, self.Baidu_Mod);
  if (Assigned(myTask)) then
  begin
    myTask.start;
    myTask.Wait(self.delay);
    if (myTask.Status = TTaskStatus.Completed) then
    begin
      Result := myTask.Value;
    end;
  end;
end;

{ TBaidu_Mod }

constructor TBaidu_Mod.Create(Aapikey: string);
begin
  self.apikey := Aapikey;
  self.timestamp := getDefauleTimestamp();
  self.device_type := Ord(dtAndroid); // 安卓s
end;

{ TApp_Create_Tag }

constructor TApp_Create_Tag.Create(ABaidu_Mod: TBaidu_Mod; Asecret_key: string; AUrl: string);
var
  url: String;
begin
  if AUrl = '' then
  begin
    url := TBaidu_Helper.BASE_URL;
  end
  else
  begin
    url := AUrl;
  end;
  url := url + TBaidu_Helper.App_Create_Tag;

  inherited Create(ABaidu_Mod, Asecret_key, url);
end;

function TApp_Create_Tag.PushMessageAndResponse: TBdResponse<TApp_Create_Tag_Response>;
var
  jsonResponse: String;
  jo: TJsonObject;
  params: TJsonObject;
begin
  // {
  // "request_id":123456789,
  // "response_params":{
  // "tag":"tag_name",
  // "result":0
  // }
  // }

  Result := TBdResponse<TApp_Create_Tag_Response>.Create(TApp_Create_Tag_Response.Create());

  jsonResponse := PushMessage();
  if (not string.IsNullOrEmpty(jsonResponse)) then
  begin
    jo := TJsonObject.Parse(jsonResponse) as TJsonObject;
    try
      Result.request_id := jo.i['request_id'];
      Result.error_code := jo.i['error_code'];
      Result.error_msg := jo.S['error_msg'];
      params := jo.O['response_params'];
      if Assigned(params) then
      begin
        Result.response_params.tag := params.S['tag'];
        Result.response_params.Result := params.i['result'];
      end;
    finally
      jo.Free;
    end;
  end;
end;

{ TReport_Query_Msg_Status_Mod }

constructor TReport_Query_Msg_Status_Mod.Create(Aapikey, Amsg_id: String);
begin
  inherited Create(Aapikey);
  self.msg_id := Amsg_id;
end;

{ TApp_Create_Tag_Mod }

constructor TApp_Create_Tag_Mod.Create(Aapikey, Atag: String);
begin
  inherited Create(Aapikey);
  self.tag := Atag;
end;

{ TApp_Query_Tags_Mod }

constructor TApp_Query_Tags_Mod.Create(Aapikey: String; Astart, Alimit: Cardinal);
begin
  inherited Create(Aapikey);
  self.start := Astart;
  self.limit := Alimit;
end;

constructor TApp_Query_Tags_Mod.Create(Aapikey, Atag: String);
begin
  inherited Create(Aapikey, Atag);
  self.start := 0;
  self.limit := 100;
end;

{ TApp_Query_Tags }

constructor TApp_Query_Tags.Create(ABaidu_Mod: TBaidu_Mod; Asecret_key, AUrl: string);
var
  url: String;
begin
  if AUrl = '' then
  begin
    url := TBaidu_Helper.BASE_URL;
  end
  else
  begin
    url := AUrl;
  end;
  url := url + TBaidu_Helper.APP_QUERY_TAGS;

  inherited Create(ABaidu_Mod, Asecret_key, url);

end;

function TApp_Query_Tags.PushMessageAndResponse: TBdResponse<TApp_Query_Tags_Response>;
var
  jsonResponse: String;
  jo: TJsonObject;
  params: TJsonObject;
  resultArray: TJsonArray;
  i: Integer;
  statusArray: Tquery_tags_array;
begin
  // {
  // "request_id":123456789,
  // "response_params":{
  // "total_num":100,
  // "tags":[
  // {
  // "tid":1000,
  // "tag":"tag1",
  // "info":"tag1",
  // "type":0,
  // "createtime":140233424
  // },
  // ...
  // ]
  // }
  // }

  Result := TBdResponse<TApp_Query_Tags_Response>.Create(TApp_Query_Tags_Response.Create());

  jsonResponse := PushMessage();
  if (not string.IsNullOrEmpty(jsonResponse)) then
  begin
    jo := TJsonObject.Parse(jsonResponse) as TJsonObject;
    try
      Result.request_id := jo.i['request_id'];
      Result.error_code := jo.i['error_code'];
      Result.error_msg := jo.S['error_msg'];
      params := jo.O['response_params'];
      if Assigned(params) then
      begin
        Result.response_params.total_num := params.i['total_num'];
        resultArray := params.ExtractArray('tags');
        if (Assigned(resultArray)) then
        begin
          for i := 0 to resultArray.Count - 1 do
          begin
            if resultArray.Items[i].Typ = jdtObject then
            begin
              statusArray := Tquery_tags_array.Create();
              statusArray.tid := resultArray.Items[i].ObjectValue.i['tid'];
              statusArray.tag := resultArray.Items[i].ObjectValue.S['tag'];
              statusArray.info := resultArray.Items[i].ObjectValue.S['info'];
              statusArray.iType := resultArray.Items[i].ObjectValue.i['type'];
              statusArray.createtime := resultArray.Items[i].ObjectValue.i['createtime'];
              Result.response_params.Result.Add(statusArray);
            end;
          end;
        end;
      end;
    finally
      jo.Free;
    end;
  end;
end;

{ TApp_Del_Tag }

constructor TApp_Del_Tag.Create(ABaidu_Mod: TBaidu_Mod; Asecret_key, AUrl: string);
var
  url: String;
begin
  if AUrl = '' then
  begin
    url := TBaidu_Helper.BASE_URL;
  end
  else
  begin
    url := AUrl;
  end;
  url := url + TBaidu_Helper.APP_DEL_TAG;

  inherited Create(ABaidu_Mod, Asecret_key, url);

end;

function TApp_Del_Tag.PushMessageAndResponse: TBdResponse<TApp_Del_Tag_Response>;
var
  jsonResponse: String;
  jo: TJsonObject;
  params: TJsonObject;
begin
  // {
  // "request_id":123456789,
  // "response_params":{
  // "tag":"tag_name",
  // "result":0
  // }
  // }

  Result := TBdResponse<TApp_Del_Tag_Response>.Create(TApp_Del_Tag_Response.Create());

  jsonResponse := PushMessage();
  if (not string.IsNullOrEmpty(jsonResponse)) then
  begin
    jo := TJsonObject.Parse(jsonResponse) as TJsonObject;
    try
      Result.request_id := jo.i['request_id'];
      Result.error_code := jo.i['error_code'];
      Result.error_msg := jo.S['error_msg'];
      params := jo.O['response_params'];
      if Assigned(params) then
      begin
        Result.response_params.tag := params.S['tag'];
        Result.response_params.Result := params.i['result'];
      end;
    finally
      jo.Free;
    end;
  end;
end;

{ TPush_All_Mod }

constructor TPush_All_Mod.Create(Aapikey, Amsg: String);
begin
  inherited Create(Aapikey);
  self.msg := Amsg;
  self.msg_type := Ord(mtMessage); // 消息类型
  self.msg_expires := 604800; // 7天过期
  self.Deploy_Status := Ord(dsProduct); // 生产状态
end;

constructor TPush_All_Mod.Create(Aapikey, Amsg: String; Amsg_type: Cardinal);
begin
  Create(Aapikey, Amsg);

  self.msg_type := Amsg_type; // 消息类型
  self.msg_expires := 604800; // 7天过期
  self.Deploy_Status := Ord(dsProduct); // 生产状态
end;

{ TPush_All }

constructor TPush_All.Create(ABaidu_Mod: TBaidu_Mod; Asecret_key, AUrl: string);
var
  url: String;
begin
  if AUrl = '' then
  begin
    url := TBaidu_Helper.BASE_URL;
  end
  else
  begin
    url := AUrl;
  end;
  url := url + TBaidu_Helper.PUSH_ALL;

  inherited Create(ABaidu_Mod, Asecret_key, url);

end;

function TPush_All.PushMessageAndResponse: TBdResponse<TPush_All_Response>;
var
  jsonResponse: String;
  jo: TJsonObject;
  params: TJsonObject;
begin
  // {
  // "request_id":123456789,
  // "response_params":
  // {
  // "msg_id":"24234532453245",
  // "send_time":1428156644
  // }
  // }

  Result := TBdResponse<TPush_All_Response>.Create(TPush_All_Response.Create());

  jsonResponse := PushMessage();
  if (not string.IsNullOrEmpty(jsonResponse)) then
  begin
    jo := TJsonObject.Parse(jsonResponse) as TJsonObject;
    try
      Result.request_id := jo.i['request_id'];
      Result.error_code := jo.i['error_code'];
      Result.error_msg := jo.S['error_msg'];
      params := jo.O['response_params'];
      if Assigned(params) then
      begin
        Result.response_params.msg_id := params.S['msg_id'];
        Result.response_params.send_time := params.i['send_time'];
        Result.response_params.timer_id := params.S['timer_id'];
      end;
    finally
      jo.Free;
    end;
  end;
end;

{ TPush_Batch_Device_Mod }

constructor TPush_Batch_Device_Mod.Create(Aapikey, Achannel_ids, Amsg, Atopic_id: String);
begin
  Create(apikey, Achannel_ids, Amsg, Atopic_id, Ord(mtMessage));
end;

constructor TPush_Batch_Device_Mod.Create(Aapikey, Achannel_ids, Amsg, Atopic_id: String; msg_type: Cardinal);
begin
  inherited Create(Aapikey);
  self.channel_ids := Achannel_ids;
  self.msg := Amsg;
  self.msg_type := msg_type; // 消息
  self.msg_expires := 86400; // 1天过期（只能一天）

  self.topic_id := Atopic_id; // 主题
end;

{ TPush_Batch_Device }

constructor TPush_Batch_Device.Create(ABaidu_Mod: TBaidu_Mod; Asecret_key, AUrl: string);
var
  url: String;
begin
  if AUrl = '' then
  begin
    url := TBaidu_Helper.BASE_URL;
  end
  else
  begin
    url := AUrl;
  end;
  url := url + TBaidu_Helper.PUSH_BATCH_DEVICE;

  inherited Create(ABaidu_Mod, Asecret_key, url);

end;

function TPush_Batch_Device.PushMessageAndResponse: TBdResponse<TPush_Batch_Device_Response>;
var
  jsonResponse: String;
  jo: TJsonObject;
  params: TJsonObject;
begin
  // {
  // "request_id":123456789,
  // "response_params":
  // {
  // "msg_id":"24234532453245",
  // "send_time":1428156644
  // }
  // }

  Result := TBdResponse<TPush_Batch_Device_Response>.Create(TPush_Batch_Device_Response.Create());

  jsonResponse := PushMessage();
  if (not string.IsNullOrEmpty(jsonResponse)) then
  begin
    jo := TJsonObject.Parse(jsonResponse) as TJsonObject;
    try
      Result.request_id := jo.i['request_id'];
      Result.error_code := jo.i['error_code'];
      Result.error_msg := jo.S['error_msg'];
      params := jo.O['response_params'];
      if Assigned(params) then
      begin
        Result.response_params.msg_id := params.S['msg_id'];
        Result.response_params.send_time := params.i['send_time'];
      end;
    finally
      jo.Free;
    end;
  end;
end;

{ TPush_Single_Device_Mod }

constructor TPush_Single_Device_Mod.Create(Aapikey, Achannel_id, Amsg: String);
begin
  Create(Aapikey, Achannel_id, Amsg, Ord(mtMessage));
end;

constructor TPush_Single_Device_Mod.Create(Aapikey, Achannel_id, Amsg: String; Amsg_type: Cardinal);
begin
  inherited Create(Aapikey);
  self.channel_id := Achannel_id;
  self.msg := Amsg;
  self.msg_type := Amsg_type; // 消息类型
  self.msg_expires := 604800; // 7天过期
  self.Deploy_Status := Ord(dsProduct); // 生产状态
end;

{ TPush_Single_Device }

constructor TPush_Single_Device.Create(ABaidu_Mod: TBaidu_Mod; Asecret_key, AUrl: string);
var
  url: String;
begin
  if AUrl = '' then
  begin
    url := TBaidu_Helper.BASE_URL;
  end
  else
  begin
    url := AUrl;
  end;
  url := url + TBaidu_Helper.PUSH_SIGNLE_DEVICE;

  inherited Create(ABaidu_Mod, Asecret_key, url);

end;

function TPush_Single_Device.PushMessageAndResponse: TBdResponse<TPush_Single_Device_Response>;
var
  jsonResponse: String;
  jo: TJsonObject;
  params: TJsonObject;
begin
  // {
  // "request_id":123456789,
  // "response_params":
  // {
  // "msg_id":"24234532453245",
  // "send_time":1428156644
  // }
  // }

  Result := TBdResponse<TPush_Single_Device_Response>.Create(TPush_Single_Device_Response.Create());

  jsonResponse := PushMessage();
  if (not string.IsNullOrEmpty(jsonResponse)) then
  begin
    jo := TJsonObject.Parse(jsonResponse) as TJsonObject;
    try
      Result.request_id := jo.i['request_id'];
      Result.error_code := jo.i['error_code'];
      Result.error_msg := jo.S['error_msg'];
      params := jo.O['response_params'];
      if Assigned(params) then
      begin
        Result.response_params.msg_id := params.S['msg_id'];
        Result.response_params.send_time := params.i['send_time'];
      end;
    finally
      jo.Free;
    end;
  end;
end;

{ TPush_Tags_Mod }

constructor TPush_Tags_Mod.Create(Aapikey, Atag, Amsg: String);
begin
  Create(Aapikey, Atag, Amsg, Ord(mtMessage));
end;

constructor TPush_Tags_Mod.Create(Aapikey, Atag, Amsg: String; Amsg_type: Cardinal);
begin
  inherited Create(Aapikey);
  self.pushtype := 1; // 目前固定值为 1
  self.tag := Atag;
  self.msg := Amsg;

  self.msg_type := Amsg_type; // 消息类型
  self.msg_expires := 604800; // 7天过期
  self.Deploy_Status := Ord(dsProduct); // 生产状态
end;

{ TPush_Tags }

constructor TPush_Tags.Create(ABaidu_Mod: TBaidu_Mod; Asecret_key, AUrl: string);
var
  url: String;
begin
  if AUrl = '' then
  begin
    url := TBaidu_Helper.BASE_URL;
  end
  else
  begin
    url := AUrl;
  end;
  url := url + TBaidu_Helper.PUSH_TAGS;

  inherited Create(ABaidu_Mod, Asecret_key, url);

end;

function TPush_Tags.PushMessageAndResponse: TBdResponse<TPush_Tags_Response>;
var
  jsonResponse: String;
  jo: TJsonObject;
  params: TJsonObject;
begin
  // {
  // "request_id":123456789,
  // "response_params":
  // {
  // "msg_id":"24234532453245",
  // "send_time":1428156644
  // }
  // }

  Result := TBdResponse<TPush_Tags_Response>.Create(TPush_Tags_Response.Create());

  jsonResponse := PushMessage();
  if (not string.IsNullOrEmpty(jsonResponse)) then
  begin
    jo := TJsonObject.Parse(jsonResponse) as TJsonObject;
    try
      Result.request_id := jo.i['request_id'];
      Result.error_code := jo.i['error_code'];
      Result.error_msg := jo.S['error_msg'];
      params := jo.O['response_params'];
      if Assigned(params) then
      begin
        Result.response_params.msg_id := params.S['msg_id'];
        Result.response_params.send_time := params.i['send_time'];
        Result.response_params.timer_id := params.S['timer_id'];
      end;
    finally
      jo.Free;
    end;
  end;
end;

{ TReport_Query_Timer_Records_Mod }

constructor TReport_Query_Timer_Records_Mod.Create(Aapikey, Atimer_id: String);
begin
  inherited Create(Aapikey);
  self.timer_id := Atimer_id;
  self.start := 0;
  self.limit := 100;
end;

{ TReport_Query_Timer_Records }

constructor TReport_Query_Timer_Records.Create(ABaidu_Mod: TBaidu_Mod; Asecret_key, AUrl: string);
var
  url: String;
begin
  if AUrl = '' then
  begin
    url := TBaidu_Helper.BASE_URL;
  end
  else
  begin
    url := AUrl;
  end;
  url := url + TBaidu_Helper.REPORT_QUERY_TIMER_RECORDS;

  inherited Create(ABaidu_Mod, Asecret_key, url);

end;

function TReport_Query_Timer_Records.PushMessageAndResponse
  : TBdResponse<TReport_Query_Timer_Records_Response>;
var
  jsonResponse: String;
  jo: TJsonObject;
  params: TJsonObject;
  resultArray: TJsonArray;
  i: Integer;
  statusArray: Tquery_timer_records_array;

begin
  // {
  // "request_id":123456789,
  // "response_params":
  // {
  // "timer_id":1011,
  // "result":[
  // {
  // "msg_id":"24234532453245",
  // "status":0,
  // "send_time":147456789
  // },
  // ......
  // ]
  // }
  // }

  Result := TBdResponse<TReport_Query_Timer_Records_Response>.Create
    (TReport_Query_Timer_Records_Response.Create());

  jsonResponse := PushMessage();
  if (not string.IsNullOrEmpty(jsonResponse)) then
  begin
    jo := TJsonObject.Parse(jsonResponse) as TJsonObject;
    try
      Result.request_id := jo.i['request_id'];
      Result.error_code := jo.i['error_code'];
      Result.error_msg := jo.S['error_msg'];
      params := jo.O['response_params'];
      if Assigned(params) then
      begin
        Result.response_params.timer_id := params.S['timer_id'];
        resultArray := params.ExtractArray('result');
        if (Assigned(resultArray)) then
        begin
          for i := 0 to resultArray.Count - 1 do
          begin
            if resultArray.Items[i].Typ = jdtObject then
            begin
              statusArray := Tquery_timer_records_array.Create();
              statusArray.Status := resultArray.Items[i].ObjectValue.i['status'];
              statusArray.send_time := resultArray.Items[i].ObjectValue.i['send_time'];
              statusArray.msg_id := resultArray.Items[i].ObjectValue.S['msg_id'];
              Result.response_params.Result.Add(statusArray);
            end;
          end;
        end;
      end;
    finally
      jo.Free;
    end;
  end;
end;

{ TReport_Query_Topic_Records_Mod }

constructor TReport_Query_Topic_Records_Mod.Create(Aapikey, Atopic_id: String);
begin
  inherited Create(Aapikey);
  self.topic_id := Atopic_id;
  self.start := 0;
  self.limit := 100;
end;

{ TReport_Query_Topic_Records }

constructor TReport_Query_Topic_Records.Create(ABaidu_Mod: TBaidu_Mod; Asecret_key, AUrl: string);
var
  url: String;
begin
  if AUrl = '' then
  begin
    url := TBaidu_Helper.BASE_URL;
  end
  else
  begin
    url := AUrl;
  end;
  url := url + TBaidu_Helper.REPORT_QUERY_TOPIC_RECORDS;

  inherited Create(ABaidu_Mod, Asecret_key, url);

end;

function TReport_Query_Topic_Records.PushMessageAndResponse
  : TBdResponse<TReport_Query_Topic_Records_Response>;
var
  jsonResponse: String;
  jo: TJsonObject;
  params: TJsonObject;
  resultArray: TJsonArray;
  i: Integer;
  statusArray: Tquery_topic_records_array;
begin
  // {
  // "request_id":123456789,
  // "response_params":
  // {
  // "topic_id":"1011",
  // "result":[
  // {
  // "msg_id":"24234532453245",
  // "status":0,
  // "send_time":147456789
  // },
  // …
  // ]
  // }
  // }

  Result := TBdResponse<TReport_Query_Topic_Records_Response>.Create
    (TReport_Query_Topic_Records_Response.Create());

  jsonResponse := PushMessage();
  if (not string.IsNullOrEmpty(jsonResponse)) then
  begin
    jo := TJsonObject.Parse(jsonResponse) as TJsonObject;
    try
      Result.request_id := jo.i['request_id'];
      Result.error_code := jo.i['error_code'];
      Result.error_msg := jo.S['error_msg'];
      params := jo.O['response_params'];
      if Assigned(params) then
      begin
        Result.response_params.topic_id := params.S['topic_id'];
        resultArray := params.ExtractArray('result');
        if (Assigned(resultArray)) then
        begin
          for i := 0 to resultArray.Count - 1 do
          begin
            if resultArray.Items[i].Typ = jdtObject then
            begin
              statusArray := Tquery_topic_records_array.Create();
              statusArray.Status := resultArray.Items[i].ObjectValue.i['status'];
              statusArray.send_time := resultArray.Items[i].ObjectValue.i['send_time'];
              statusArray.msg_id := resultArray.Items[i].ObjectValue.S['msg_id'];
              Result.response_params.Result.Add(statusArray);
            end;
          end;
        end;
      end;
    finally
      jo.Free;
    end;
  end;
end;
{ TReport_Statistic_Device }

constructor TReport_Statistic_Device.Create(ABaidu_Mod: TBaidu_Mod; Asecret_key, AUrl: string);
var
  url: String;
begin
  if AUrl = '' then
  begin
    url := TBaidu_Helper.BASE_URL;
  end
  else
  begin
    url := AUrl;
  end;
  url := url + TBaidu_Helper.REPORT_STATISTIC_DEVICE;

  inherited Create(ABaidu_Mod, Asecret_key, url);

end;

{ TReport_Statistic_Msg }

constructor TReport_Statistic_Msg<T>.Create(ABaidu_Mod: TBaidu_Mod; Asecret_key, AUrl: string);
var
  url: String;
begin
  if AUrl = '' then
  begin
    url := TBaidu_Helper.BASE_URL;
  end
  else
  begin
    url := AUrl;
  end;
  url := url + TBaidu_Helper.REPORT_STATISTIC_MSG;

  inherited Create(ABaidu_Mod, Asecret_key, url);

end;

function TReport_Statistic_Device.PushMessageAndResponse: TBdResponse<TReport_Statistic_Device_Response>;
var
  jsonResponse: String;
  jo: TJsonObject;
  params: TJsonObject;
  resultArray: TJsonObject;
  mapObj: TJsonObject;
  i: Integer;

  statusArray: TReport_Statistic_Device_Map_Value;
  Key: Cardinal;
begin
  // {
  // "request_id" : 500348335,
  // "response_params" : {
  // "total_num" : 180,
  // "result" : {
  // "1429977600" : {
  // "new_term" : 2,
  // "del_term" : 115,
  // "online_term" : 178,
  // "addup_term" : 103,
  // "total_term" : 193
  // },
  // ...
  // }
  // }
  // };
  Result := TBdResponse<TReport_Statistic_Device_Response>.Create(TReport_Statistic_Device_Response.Create());

  jsonResponse := PushMessage();
  if (not string.IsNullOrEmpty(jsonResponse)) then
  begin
    jo := TJsonObject.Parse(jsonResponse) as TJsonObject;
    try
      Result.request_id := jo.i['request_id'];
      Result.error_code := jo.i['error_code'];
      Result.error_msg := jo.S['error_msg'];
      params := jo.O['response_params'];
      if Assigned(params) then
      begin
        Result.response_params.total_num := params.i['total_num'];
        resultArray := params.O['result'];
        if (Assigned(resultArray)) then
        begin
          for i := 0 to resultArray.Count - 1 do
          begin
            if resultArray.Items[i].Typ = jdtObject then
            begin
              statusArray := TReport_Statistic_Device_Map_Value.Create();

              mapObj := resultArray.Items[i].ObjectValue;
              statusArray.new_term := mapObj.i['new_term'];
              statusArray.del_term := mapObj.i['del_term'];
              statusArray.online_term := mapObj.i['online_term'];
              statusArray.addup_term := mapObj.i['addup_term'];
              statusArray.total_term := mapObj.i['total_term'];

              Key := StrToIntDef(resultArray.Names[i], 0);

              if Key > 0 then
                Result.response_params.Result.Add(Key, statusArray);
            end;
          end;
        end;
      end;
    finally
      jo.Free;
    end;
  end;
end;

{ TReport_Statistic_Topic_Mod }

constructor TReport_Statistic_Topic_Mod.Create(Aapikey, Atopic_id: String);
begin
  inherited Create(Aapikey);
  self.topic_id := Atopic_id;

end;

{ TReport_Statistic_Topic }

constructor TReport_Statistic_Topic.Create(ABaidu_Mod: TBaidu_Mod; Asecret_key, AUrl: string);
var
  url: String;
begin
  if AUrl = '' then
  begin
    url := TBaidu_Helper.BASE_URL;
  end
  else
  begin
    url := AUrl;
  end;
  url := url + TBaidu_Helper.REPORT_STATISTIC_TOPIC;

  inherited Create(ABaidu_Mod, Asecret_key, url);

end;

function TReport_Statistic_Topic.PushMessageAndResponse: TBdResponse<TReport_Statistic_Topic_Response>;
var
  jsonResponse: String;
  jo: TJsonObject;
  params: TJsonObject;
  resultArray: TJsonObject;
  mapObj: TJsonObject;
  i: Integer;
  statusArray: TReport_Statistic_Topic_Map_Value;
  Key: Cardinal;
begin
  // {
  // "request_id" : 500335275,
  // "response_params" : {
  // "total_num" : 120,
  // "result" : {
  // "1429977600" : {
  // "ack" : 111
  // },
  // ...
  // }
  // }
  // }

  Result := TBdResponse<TReport_Statistic_Topic_Response>.Create(TReport_Statistic_Topic_Response.Create());

  jsonResponse := PushMessage();
  if (not string.IsNullOrEmpty(jsonResponse)) then
  begin
    jo := TJsonObject.Parse(jsonResponse) as TJsonObject;
    try
      Result.request_id := jo.i['request_id'];
      Result.error_code := jo.i['error_code'];
      Result.error_msg := jo.S['error_msg'];
      params := jo.O['response_params'];
      if Assigned(params) then
      begin
        Result.response_params.total_num := params.i['total_num'];
        resultArray := params.O['result'];
        if (Assigned(resultArray)) then
        begin
          for i := 0 to resultArray.Count - 1 do
          begin
            if resultArray.Items[i].Typ = jdtObject then
            begin
              statusArray := TReport_Statistic_Topic_Map_Value.Create();

              mapObj := resultArray.Items[i].ObjectValue;
              statusArray.ack := mapObj.i['ack'];
              Key := StrToIntDef(resultArray.Names[i], 0);

              if Key > 0 then
                Result.response_params.Result.Add(Key, statusArray);
            end;
          end;
        end;
      end;
    finally
      jo.Free;
    end;
  end;
end;

{ TTag_Add_Devices_Mod }

constructor TTag_Add_Devices_Mod.Create(Aapikey, Atag, Achannel_ids: String);
begin
  inherited Create(Aapikey);
  self.tag := Atag;
  self.channel_ids := Achannel_ids;
end;

{ TTag_Add_Devices }

constructor TTag_Add_Devices.Create(ABaidu_Mod: TBaidu_Mod; Asecret_key, AUrl: string);
var
  url: String;
begin
  if AUrl = '' then
  begin
    url := TBaidu_Helper.BASE_URL;
  end
  else
  begin
    url := AUrl;
  end;
  url := url + TBaidu_Helper.TAG_ADD_DEVICES;

  inherited Create(ABaidu_Mod, Asecret_key, url);

end;

function TTag_Add_Devices.PushMessageAndResponse: TBdResponse<TTag_Add_Devices_Response>;
var
  jsonResponse: String;
  jo: TJsonObject;
  params: TJsonObject;
  resultArray: TJsonArray;
  i: Integer;
  statusArray: Tadd_devices_array;

begin
  // {
  // "request_id":123456789,
  // "response_params":{
  // "devices":[
  // {
  // "channel_id":"43848320940932",
  // "result":0
  // },
  // {
  // "channel_id":"48302948302432043",
  // "result":1
  // },
  // ...
  // ]
  // }
  // }

  Result := TBdResponse<TTag_Add_Devices_Response>.Create(TTag_Add_Devices_Response.Create());

  jsonResponse := PushMessage();
  if (not string.IsNullOrEmpty(jsonResponse)) then
  begin
    jo := TJsonObject.Parse(jsonResponse) as TJsonObject;
    try
      Result.request_id := jo.i['request_id'];
      Result.error_code := jo.i['error_code'];
      Result.error_msg := jo.S['error_msg'];
      params := jo.O['response_params'];
      if Assigned(params) then
      begin
        resultArray := params.ExtractArray('devices');
        if (Assigned(resultArray)) then
        begin
          for i := 0 to resultArray.Count - 1 do
          begin
            if resultArray.Items[i].Typ = jdtObject then
            begin
              statusArray := Tadd_devices_array.Create();
              statusArray.Result := resultArray.Items[i].ObjectValue.i['result'];
              statusArray.channel_id := resultArray.Items[i].ObjectValue.S['channel_id'];
              Result.response_params.Result.Add(statusArray);
            end;
          end;
        end;
      end;
    finally
      jo.Free;
    end;
  end;
end;

{ TTag_Del_Devices }

constructor TTag_Del_Devices.Create(ABaidu_Mod: TBaidu_Mod; Asecret_key, AUrl: string);
var
  url: String;
begin
  if AUrl = '' then
  begin
    url := TBaidu_Helper.BASE_URL;
  end
  else
  begin
    url := AUrl;
  end;
  url := url + TBaidu_Helper.TAG_DEL_DEVICES;

  inherited Create(ABaidu_Mod, Asecret_key, url);

end;

function TTag_Del_Devices.PushMessageAndResponse: TBdResponse<TTagDel_devices_response>;
var
  jsonResponse: String;
  jo: TJsonObject;
  params: TJsonObject;
  resultArray: TJsonArray;
  i: Integer;
  statusArray: Tdel_devices_array;

begin
  // {
  // "request_id":123456789,
  // "response_params":{
  // "devices":[
  // {
  // "channel_id":"43848320940932",
  // "result":0
  // },
  // {
  // "channel_id":"48302948302432043",
  // "result":1
  // },
  // ...
  // ]
  // }
  // }

  Result := TBdResponse<TTagDel_devices_response>.Create(TTagDel_devices_response.Create());

  jsonResponse := PushMessage();
  if (not string.IsNullOrEmpty(jsonResponse)) then
  begin
    jo := TJsonObject.Parse(jsonResponse) as TJsonObject;
    try
      Result.request_id := jo.i['request_id'];
      Result.error_code := jo.i['error_code'];
      Result.error_msg := jo.S['error_msg'];
      params := jo.O['response_params'];
      if Assigned(params) then
      begin
        resultArray := params.ExtractArray('devices');
        if (Assigned(resultArray)) then
        begin
          for i := 0 to resultArray.Count - 1 do
          begin
            if resultArray.Items[i].Typ = jdtObject then
            begin
              statusArray := Tdel_devices_array.Create();
              statusArray.Result := resultArray.Items[i].ObjectValue.i['result'];
              statusArray.channel_id := resultArray.Items[i].ObjectValue.S['channel_id'];
              Result.response_params.Result.Add(statusArray);
            end;
          end;
        end;
      end;
    finally
      jo.Free;
    end;
  end;
end;

{ TTag_Device_Num_Mod }

constructor TTag_Device_Num_Mod.Create(Aapikey, Atag: String);
begin
  inherited Create(Aapikey);
  self.tag := Atag;
end;

{ TTag_Device_Num }

constructor TTag_Device_Num.Create(ABaidu_Mod: TBaidu_Mod; Asecret_key, AUrl: string);
var
  url: String;
begin
  if AUrl = '' then
  begin
    url := TBaidu_Helper.BASE_URL;
  end
  else
  begin
    url := AUrl;
  end;
  url := url + TBaidu_Helper.TAG_DEVICE_NUM;

  inherited Create(ABaidu_Mod, Asecret_key, url);

end;

function TTag_Device_Num.PushMessageAndResponse: TBdResponse<TTag_Device_Num_Response>;
var
  jsonResponse: String;
  jo: TJsonObject;
  params: TJsonObject;
begin
  // {
  // "request_id":123456789,
  // "response_params":{
  // "device_num":1000
  // }
  // }

  Result := TBdResponse<TTag_Device_Num_Response>.Create(TTag_Device_Num_Response.Create());

  jsonResponse := PushMessage();
  if (not string.IsNullOrEmpty(jsonResponse)) then
  begin
    jo := TJsonObject.Parse(jsonResponse) as TJsonObject;
    try
      Result.request_id := jo.i['request_id'];
      Result.error_code := jo.i['error_code'];
      Result.error_msg := jo.S['error_msg'];
      params := jo.O['response_params'];
      if Assigned(params) then
      begin
        Result.response_params.device_num := params.i['device_num'];
      end;
    finally
      jo.Free;
    end;
  end;
end;

{ TTimer_Cancel_Mod }

constructor TTimer_Cancel_Mod.Create(Aapikey, Atimer_id: String);
begin
  inherited Create(Aapikey);
  self.timer_id := Atimer_id;
end;

{ TTimer_Cancel }

constructor TTimer_Cancel.Create(ABaidu_Mod: TBaidu_Mod; Asecret_key, AUrl: string);
var
  url: String;
begin
  if AUrl = '' then
  begin
    url := TBaidu_Helper.BASE_URL;
  end
  else
  begin
    url := AUrl;
  end;
  url := url + TBaidu_Helper.TIMER_CANCEL;

  inherited Create(ABaidu_Mod, Asecret_key, url);

end;

function TTimer_Cancel.PushMessageAndResponse: TBdResponse<TTimer_Cancel_Response>;
var
  jsonResponse: String;
  jo: TJsonObject;
begin
  // {
  // "request_id":12394838223,
  // }
  Result := TBdResponse<TTimer_Cancel_Response>.Create(TTimer_Cancel_Response.Create());

  jsonResponse := PushMessage();
  if (not string.IsNullOrEmpty(jsonResponse)) then
  begin
    jo := TJsonObject.Parse(jsonResponse) as TJsonObject;
    try
      Result.request_id := jo.i['request_id'];
      Result.error_code := jo.i['error_code'];
      Result.error_msg := jo.S['error_msg'];
    finally
      jo.Free;
    end;
  end;
end;

{ TTimer_Query_List_Mod }

constructor TTimer_Query_List_Mod.Create(Aapikey, Atimer_id: String);
begin
  inherited Create(Aapikey);
  self.timer_id := Atimer_id;
end;

{ TTimer_Query_List }

constructor TTimer_Query_List.Create(ABaidu_Mod: TBaidu_Mod; Asecret_key, AUrl: string);
var
  url: String;
begin
  if AUrl = '' then
  begin
    url := TBaidu_Helper.BASE_URL;
  end
  else
  begin
    url := AUrl;
  end;
  url := url + TBaidu_Helper.TIMER_QUERY_LIST;

  inherited Create(ABaidu_Mod, Asecret_key, url);

end;

function TTimer_Query_List.PushMessageAndResponse: TBdResponse<TTimer_Query_List_Response>;
var
  jsonResponse: String;
  jo: TJsonObject;
  params: TJsonObject;
  resultArray: TJsonArray;
  i: Integer;
  statusArray: Ttimer_query_list_array;

begin
  // {
  // "request_id":123456789,
  // "response_params":{
  // "total_num":10,
  // "result":[
  // {
  // "timer_id":1000,
  // "msg":"message body",
  // "msg_type":0,
  // "range_type":2,
  // "send_time":147456789
  // },
  // ...
  // ]
  // }
  // }

  Result := TBdResponse<TTimer_Query_List_Response>.Create(TTimer_Query_List_Response.Create());

  jsonResponse := PushMessage();
  if (not string.IsNullOrEmpty(jsonResponse)) then
  begin
    jo := TJsonObject.Parse(jsonResponse) as TJsonObject;
    try
      Result.request_id := jo.i['request_id'];
      Result.error_code := jo.i['error_code'];
      Result.error_msg := jo.S['error_msg'];
      params := jo.O['response_params'];
      if Assigned(params) then
      begin
        Result.response_params.total_num := params.i['total_num'];
        resultArray := params.ExtractArray('result');
        if (Assigned(resultArray)) then
        begin
          for i := 0 to resultArray.Count - 1 do
          begin
            if resultArray.Items[i].Typ = jdtObject then
            begin
              statusArray := Ttimer_query_list_array.Create();
              statusArray.timer_id := resultArray.Items[i].ObjectValue.S['timer_id'];
              statusArray.msg := resultArray.Items[i].ObjectValue.S['msg'];
              statusArray.msg_type := resultArray.Items[i].ObjectValue.i['msg_type'];
              statusArray.range_type := resultArray.Items[i].ObjectValue.i['range_type'];
              statusArray.send_time := resultArray.Items[i].ObjectValue.i['send_time'];
              Result.response_params.Result.Add(statusArray);
            end;
          end;
        end;
      end;
    finally
      jo.Free;
    end;
  end;
end;

{ TTopic_Query_List }

constructor TTopic_Query_List.Create(ABaidu_Mod: TBaidu_Mod; Asecret_key, AUrl: string);
var
  url: String;
begin
  if AUrl = '' then
  begin
    url := TBaidu_Helper.BASE_URL;
  end
  else
  begin
    url := AUrl;
  end;
  url := url + TBaidu_Helper.TOPIC_QUERY_LIST;

  inherited Create(ABaidu_Mod, Asecret_key, url);

end;

function TReport_Query_Msg_Status.PushMessageAndResponse: TBdResponse<TReport_Query_Msg_Status_Response>;
var
  jsonResponse: String;
  jo: TJsonObject;
  params: TJsonObject;
  resultArray: TJsonArray;
  i: Integer;
  statusArray: Tquery_msg_status_array;

begin
  // {
  // "request_id":123456789,
  // "response_params":{
  // total_num:10,
  // result:[{
  // "msg_id":"24234532453245",
  // "status":0,
  // "success":10000,
  // "send_time":1472354868
  // }]
  // }
  // }

  Result := TBdResponse<TReport_Query_Msg_Status_Response>.Create(TReport_Query_Msg_Status_Response.Create());

  jsonResponse := PushMessage();
  if (not string.IsNullOrEmpty(jsonResponse)) then
  begin
    jo := TJsonObject.Parse(jsonResponse) as TJsonObject;
    try
      Result.request_id := jo.i['request_id'];
      Result.error_code := jo.i['error_code'];
      Result.error_msg := jo.S['error_msg'];
      params := jo.O['response_params'];
      if Assigned(params) then
      begin
        Result.response_params.total_num := params.i['total_num'];
        resultArray := params.ExtractArray('result');
        if (Assigned(resultArray)) then
        begin
          for i := 0 to resultArray.Count - 1 do
          begin
            if resultArray.Items[i].Typ = jdtObject then
            begin
              statusArray := Tquery_msg_status_array.Create();
              statusArray.Status := resultArray.Items[i].ObjectValue.i['status'];
              statusArray.success := resultArray.Items[i].ObjectValue.i['success'];
              statusArray.send_time := resultArray.Items[i].ObjectValue.i['send_time'];
              statusArray.msg_id := resultArray.Items[i].ObjectValue.S['msg_id'];
              Result.response_params.Result.Add(statusArray);
            end;
          end;
        end;
      end;
    finally
      jo.Free;
    end;
  end;
end;

function TReport_Statistic_Msg<T>.PushMessageAndResponse: TBdResponse<T>;
begin

end;

function TTopic_Query_List.PushMessageAndResponse: TBdResponse<TTopic_Query_List_Response>;
var
  jsonResponse: String;
  jo: TJsonObject;
  params: TJsonObject;
  resultArray: TJsonArray;
  i: Integer;
  statusArray: TTopic_Query_List_Array;

begin
  // {
  // "request_id":123456789,
  // "response_params":{
  // "total_num":10,
  // "topics":[
  // {
  // "topic_id":"1000",
  // "ctime":1421247101,
  // "mtime":1421247101,
  // "push_cnt":2000,
  // "ack_cnt":1000,
  // },
  // ...
  // ]
  // }
  // }
  // }

  Result := TBdResponse<TTopic_Query_List_Response>.Create(TTopic_Query_List_Response.Create());

  jsonResponse := PushMessage();
  if (not string.IsNullOrEmpty(jsonResponse)) then
  begin
    jo := TJsonObject.Parse(jsonResponse) as TJsonObject;
    try
      Result.request_id := jo.i['request_id'];
      Result.error_code := jo.i['error_code'];
      Result.error_msg := jo.S['error_msg'];
      params := jo.O['response_params'];
      if Assigned(params) then
      begin
        Result.response_params.total_num := params.i['total_num'];
        resultArray := params.ExtractArray('topics');
        if (Assigned(resultArray)) then
        begin
          for i := 0 to resultArray.Count - 1 do
          begin
            if resultArray.Items[i].Typ = jdtObject then
            begin
              statusArray := TTopic_Query_List_Array.Create();
              statusArray.ack_cnt := resultArray.Items[i].ObjectValue.i['ack_cnt'];
              statusArray.push_cnt := resultArray.Items[i].ObjectValue.i['push_cnt'];
              statusArray.mtime := resultArray.Items[i].ObjectValue.i['mtime'];
              statusArray.ctime := resultArray.Items[i].ObjectValue.i['ctime'];
              statusArray.topic_id := resultArray.Items[i].ObjectValue.S['topic_id'];
              Result.response_params.Result.Add(statusArray);
            end;
          end;
        end;
      end;
    finally
      jo.Free;
    end;
  end;
end;

end.
