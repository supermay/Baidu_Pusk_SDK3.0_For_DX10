@echo off
echo 	--------------------------------------
echo 	  1.退出
echo 	  2.删除全部临时文件(不包括EXE文件)
echo 	  3.删除全部临时文件(包括EXE文件)
echo 	  4.删除BAK文件
echo 	--------------------------------------
choice /T 5 /C:1234 /D 2 /M "请选择(5秒后默认选择2):"

if errorlevel 4 goto delBak
if errorlevel 3 goto delAll
if errorlevel 2 goto delPart
if errorlevel 1 goto end

:delBak
del *.bak /s
goto end

:delPart
del *.map /s
del *.log /s
del *.ddp /s
del *.dcu /s
del .\documents\*.pdb /s
del *.~* /s
del *.*~ /s
goto end

:delAll
del *.map /s
del *.log /s
del *.ddp /s
del *.dcu /s
del .\documents\*.pdb /s
del *.~* /s
del *.*~ /s
del *.exe /s
goto end

:end